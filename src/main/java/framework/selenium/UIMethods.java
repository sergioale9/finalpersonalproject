package framework.selenium;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utils.LoggerManager;
public class UIMethods {
    private static final LoggerManager log = LoggerManager.getInstance();
    private static final WebDriver driver = DriverManager.getInstance().getWebDriver();
    private static final JavascriptExecutor js = (JavascriptExecutor) driver;
    public static void moveToWebElement(WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
    }
    public static void scrollDownUntilElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
    }
}
