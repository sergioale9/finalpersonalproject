package ui.user;

import ui.BasePageObject;
import ui.user.components.HeaderMenu;

public abstract class BaseHomePage extends BasePageObject {
    public HeaderMenu headerMenu;
    public BaseHomePage() {
        headerMenu = new HeaderMenu();
    }
}
