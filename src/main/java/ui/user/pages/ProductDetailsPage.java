package ui.user.pages;

import framework.selenium.UIMethods;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ui.user.BaseHomePage;

public class ProductDetailsPage extends BaseHomePage {
    @FindBy(xpath="//form[@id='review-form']")
    WebElement reviewForm;
    @FindBy(xpath="//input[@id='name']")
    WebElement nameTextBox;
    @FindBy(xpath="//input[@id='email']")
    WebElement emailTextBox;
    @FindBy(xpath="//textarea[@id='review']")
    WebElement reviewBody;
    @FindBy(xpath="//button[@id='button-review']")
    WebElement submitReview;
    @FindBy(xpath="//input[@id='quantity']")
    WebElement quantityBox;
    @FindBy(xpath = "//div[@class='product-information']//h2")
    WebElement productInformation;
    @FindBy(xpath="//div[@class='alert-success alert']//span")
    WebElement messageSuccess;
    @FindBy(xpath="//button [@class='btn btn-default cart']")
    WebElement addCartButton;
    @FindBy(xpath="//div[@class='modal-content']")
    WebElement messageProductAdded;
    @FindBy(xpath = "//button[@class='btn btn-success close-modal btn-block']")
    WebElement acceptShoppingButton;
    public ProductDetailsPage() {
        PageFactory.initElements(driver, this);
        waitUntilPageObjectIsLoaded();
    }
    @Override
    public void waitUntilPageObjectIsLoaded() throws WebDriverException {

    }
    public boolean reviewFormIsVisible() {
        UIMethods.scrollDownUntilElement(reviewForm);
        return reviewForm.isDisplayed();
    }
    public void setNameTextBox(String name ) {
        UIMethods.scrollDownUntilElement(nameTextBox);
        nameTextBox.click();
        nameTextBox.clear();
        nameTextBox.sendKeys(name);
    }
    public void setEmailTextBox(String email) {
        UIMethods.scrollDownUntilElement(emailTextBox);
        emailTextBox.click();
        emailTextBox.clear();
        emailTextBox.sendKeys(email);
    }
    public void setReviewBody(String bodyReview) {
        UIMethods.scrollDownUntilElement(reviewBody);
        reviewBody.click();
        reviewBody.clear();
        reviewBody.sendKeys(bodyReview);
    }
    public ProductDetailsPage sendProductReview(String name, String email, String bodyReview) {
        setNameTextBox(name);
        setEmailTextBox(email);
        setReviewBody(bodyReview);
        submitReview.submit();
        return this;
    }
    public boolean messageSuccessIsVisible() {
        UIMethods.moveToWebElement(messageSuccess);
        return messageSuccess.isDisplayed();
    }
    public void addProductToCart() {
        addCartButton = wait.until(ExpectedConditions.elementToBeClickable(addCartButton));
        addCartButton.click();
    }
    public boolean messageProductAddedIsDisplay() {
        messageProductAdded = wait.until(ExpectedConditions.visibilityOf(messageProductAdded));
        return messageProductAdded.isDisplayed();
    }
    public void setQuantityBox(String quantity) {
        quantityBox.click();
        quantityBox.clear();
        quantityBox.sendKeys(quantity);
    }
    public String productInformationDetailsPage() {
        productInformation = wait.until(ExpectedConditions.visibilityOf(productInformation));
        return productInformation.getText();
    }
    public void acceptShoppMessage() {
        acceptShoppingButton.click();
    }
}
