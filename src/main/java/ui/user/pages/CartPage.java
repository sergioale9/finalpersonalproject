package ui.user.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ui.user.BaseHomePage;

public class CartPage extends BaseHomePage {
    @FindBy(xpath = "//a[@class='btn btn-default check_out']")
    WebElement checkoutButton;
    @FindBy(xpath = "//span[@id='empty_cart']")
    WebElement emptyCart;
    @FindBy(xpath = "//section[@id='cart_items']")
    WebElement cartItems;

    public CartPage() {
        PageFactory.initElements(driver, this);
        waitUntilPageObjectIsLoaded();
    }

    @Override
    public void waitUntilPageObjectIsLoaded() throws WebDriverException {
    }
    public void deleteAProductOfCart(String productId) {
        String deleteProductButton = String.format("//a[@class='cart_quantity_delete' and @data-product-id='%s']", productId);
        WebElement deleteProductButtonLocator = driver.findElement(By.xpath(deleteProductButton));
        deleteProductButtonLocator = wait.until(ExpectedConditions.elementToBeClickable(deleteProductButtonLocator));
        deleteProductButtonLocator.click();
    }
    public boolean cartIsEmptyIsVisible() {
        emptyCart = wait.until(ExpectedConditions.visibilityOf(emptyCart));
        return emptyCart.isDisplayed();
    }
    public boolean totalAmountIsDisplayed(String productId) {
        String totalPriceField = String.format("//tr[@id='product-%s']//p[@class='cart_total_price']", productId );
        WebElement totalPriceLocator = driver.findElement(By.xpath(totalPriceField));
        return totalPriceLocator.isDisplayed();
    }
    public String productDescription(String productId) {
        String productDescriptionString = String.format("//a[@href='/product_details/%s']", productId);
        WebElement productDescriptionLocator = driver.findElement(By.xpath(productDescriptionString));
        return productDescriptionLocator.getText();
    }
    public String productPrice(String productId) {
        String productPriceString = String.format("//tr[@id='product-%s']//td[@class='cart_price']", productId);
        WebElement productPriceLocator = driver.findElement(By.xpath(productPriceString));
        return productPriceLocator.getText();
    }
}
