package ui.user.pages;

import framework.selenium.UIMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ui.user.BaseHomePage;

public class ProductsPage extends BaseHomePage {
    @FindBy(id="search_product")
    WebElement searchBox;
    @FindBy(id="submit_search")
    WebElement submitSearchButton;
    @FindBy(xpath = "//div[@class='productinfo text-center']//p")
    WebElement productFound;

    public String productDescription;
    public ProductsPage() {
        PageFactory.initElements(driver, this);
        waitUntilPageObjectIsLoaded();
    }
    @Override
    public void waitUntilPageObjectIsLoaded() throws WebDriverException {
    }
    public ProductsPage setSearchBox(String argument) {
        searchBox.click();
        searchBox.clear();
        searchBox.sendKeys(argument);
        submitSearchButton.click();
        return this;
    }
    public void addProductToCart(String productId) {
        String addToCartButtonLocator = String.format("//div[@class='productinfo text-center']//child::a[@class='btn btn-default add-to-cart' and @data-product-id='%s']", productId);
        WebElement addToCartButton = driver.findElement((By.xpath(addToCartButtonLocator)));
        UIMethods.scrollDownUntilElement(addToCartButton);
        addToCartButton.click();
    }
    public String getProductDescription(String productId) {
        String productDescriptionString = String.format("//img[@src='/get_product_picture/%s']//following-sibling::p", productId);
        WebElement productDescriptionLocator = driver.findElement(By.xpath(productDescriptionString));
        return productDescriptionLocator.getText();
    }
    public String getProductPrice(String productId) {
        String productPriceString = String.format("//img[@src='/get_product_picture/%s']//following-sibling::h2", productId);
        WebElement productPriceLocator = driver.findElement(By.xpath(productPriceString));
        return productPriceLocator.getText();
    }
    public boolean searchBoxIsVisible() {
        UIMethods.scrollDownUntilElement(searchBox);
        return searchBox.isDisplayed();
    }
    public String productFound() {
        return productFound.getText();
    }
    public ProductDetailsPage productDetailsPage(String productId) {
        String productDetailsString = String.format("//a[@href='/product_details/%s']", productId);
        WebElement productDetailLocator = driver.findElement(By.xpath(productDetailsString));
        String productDescriptionString = String.format("//img[@src='/get_product_picture/%s']//following-sibling::p", productId);
        WebElement productInfoLocator = driver.findElement(By.xpath(productDescriptionString));
        productDescription = productInfoLocator.getText();
        UIMethods.scrollDownUntilElement(productDetailLocator);
        productDetailLocator.click();
        return new ProductDetailsPage();
    }
}
