package ui.user.components;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ui.BasePageObject;
import ui.user.pages.CartPage;
import ui.user.pages.ProductsPage;

public class HeaderMenu extends BasePageObject {
    @FindBy(xpath="//ul[@class='nav navbar-nav']")
    WebElement navBar;
    @FindBy(xpath="//a[@href='/products']")
    WebElement productsButton;
    @FindBy(xpath="//a[@href='/view_cart']")
    WebElement cartButton;

    public HeaderMenu() {
        PageFactory.initElements(driver, this);
        waitUntilPageObjectIsLoaded();
    }
    @Override
    public void waitUntilPageObjectIsLoaded() throws WebDriverException {
        navBar = wait.until(ExpectedConditions.visibilityOf(navBar));
    }
    public boolean navBarIsVisible() {
        return navBar.isDisplayed();
    }
    public ProductsPage goToProductPage() {
        productsButton.click();
        return new ProductsPage();
    }
    public CartPage goToCartPage() {
        cartButton.click();
        return new CartPage();
    }
}
