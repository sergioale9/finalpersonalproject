package com.jalasoft.automation.steps.cart;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.testng.Assert;
import ui.PageTransporter;
import ui.user.pages.CartPage;
import ui.user.pages.HomePage;
import ui.user.pages.ProductsPage;

public class CartSteps {
    private final PageTransporter pageTransporter;
    private CartPage cartPage;
    private final HomePage homePage;
    private ProductsPage productsPage;
    private String productDescription;
    private String productDescriptionShoppingCart;
    private String productPrice;
    private String productPriceShoppingCart;
    public CartSteps() {
        this.pageTransporter = PageTransporter.getInstance();
        homePage = pageTransporter.navigateToHomePage();
    }
    @Given("^I go to the shopping cart without add a product$")
    public void goDirectlyToShoppingCart() {
        cartPage = homePage.headerMenu.goToCartPage();
    }
    @Given("^I add at least one \"(.*?)\" to shopping cart$")
    public void addProductToCart(String productId) {
        productsPage = homePage.headerMenu.goToProductPage();
        productDescription = productsPage.getProductDescription(productId);
        productPrice = productsPage.getProductPrice(productId);
        productsPage.addProductToCart(productId);
    }
    @Then("^the shopping cart is empty$")
    public void isShoppingCartEmpty() {
        Assert.assertTrue(cartPage.cartIsEmptyIsVisible());
    }
    @Then ("^I go to the shopping cart with \"(.*?)\" added$")
    public void goToShoppingCart(String productId) {
        cartPage = productsPage.headerMenu.goToCartPage();
        productDescriptionShoppingCart = cartPage.productDescription(productId);
        productPriceShoppingCart = cartPage.productPrice(productId);
    }
    @Then("^I remove a \"(.*?)\" of the cart$")
    public void deleteAProductOfCart(String productId) {
        cartPage.deleteAProductOfCart(productId);
    }
    @Then("^the description and price of the product added is the correctly$")
    public void productDescriptionIsCorrectly() {
        Assert.assertEquals(productDescriptionShoppingCart, productDescription);
        Assert.assertEquals(productPriceShoppingCart, productPrice);
    }
    @Then("^the total amount should be displayed for the \"(.*?)\"$")
    public void totalAmountIsDisplayedCorrectly(String productId) {
        Assert.assertTrue(cartPage.totalAmountIsDisplayed(productId));
    }
}
