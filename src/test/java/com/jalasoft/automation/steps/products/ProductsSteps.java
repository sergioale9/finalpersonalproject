package com.jalasoft.automation.steps.products;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.testng.Assert;
import ui.PageTransporter;
import ui.user.pages.HomePage;
import ui.user.pages.ProductDetailsPage;
import ui.user.pages.ProductsPage;

import java.util.List;
import java.util.Map;

public class ProductsSteps {
    private ProductsPage productsPage;
    private final PageTransporter pageTransporter;
    private HomePage homePage;
    private ProductDetailsPage productDetailsPage;
    public ProductsSteps() {
        this.pageTransporter = PageTransporter.getInstance();
    }
    @Given("^I am on the home page$")
    public void goToHomePage() {
        homePage = pageTransporter.navigateToHomePage();
        Assert.assertTrue(homePage.headerMenu.navBarIsVisible());
    }
    @Given("^I go to the product page$")
    public void goToProductPage() {
        productsPage = homePage.headerMenu.goToProductPage();
    }
    @Given("^I search a product typing \"(.*?)\"$")
    public void searchAProduct(String argument) {
        productsPage = productsPage.setSearchBox(argument);
    }
    @Given("^I select a \"(.*?)\" and enter to see the details$")
    public void thePageRedirectsToDetailsPage(String productId) {
        productDetailsPage = productsPage.productDetailsPage(productId);
    }
    @Given("^I fill the review form with the following values$")
    public void fillReviewForm(DataTable dataTable) {
        List<Map<String, Object>> queryParamsList = dataTable.asMaps(String.class, Object.class);
        Map<String, Object> queryParams = queryParamsList.get(0);
        String name = (String) queryParams.get("Name");
        String email = (String) queryParams.get("Email");
        String bodyReview = (String) queryParams.get("ReviewBody");
        if (name == null) {
            name = "";
        }
        if (email == null) {
            email = "";
        }
        if (bodyReview == null) {
            bodyReview = "";
        }
        productDetailsPage = productDetailsPage.sendProductReview(name, email, bodyReview);
    }
    @Given("^I add the product to the cart")
    public void addProductToTheCart() {
        productDetailsPage.addProductToCart();
    }
    @Then("^the page redirect me to the products page$")
    public void isOnProductsPage() {
        Assert.assertTrue(productsPage.searchBoxIsVisible());
    }
    @Then("^the page return the searched product with \"(.*?)\"$")
    public void verifyProductFound(String argument) {
        Assert.assertEquals(productsPage.productFound(), argument);
    }
    @Then("^the page redirects to details of the \"(.*?)\"$")
    public void thePageOfTheProductSelected(String productId) {
        Assert.assertEquals(productsPage.productDescription, productDetailsPage.productInformationDetailsPage());
    }
    @Then("^the form is sent correctly$")
    public void theReviewIsSendCorrectly() {
        Assert.assertTrue(productDetailsPage.messageSuccessIsVisible());
    }
    @Then("^the form is not sent correctly$")
    public void theReviewIsNotSendCorrectly() {
        Assert.assertFalse(productDetailsPage.messageSuccessIsVisible());
    }
    @Then("^the product is added correctly$")
    public void productAddedCorrectlyToCart() {
        Assert.assertTrue(productDetailsPage.messageProductAddedIsDisplay());
        productDetailsPage.acceptShoppMessage();
    }
    @Then("^I set the quantity of products with the \"(.*?)\"$")
    public void setQuantityOfProducts(String value) {
        productDetailsPage.setQuantityBox(value);
    }
    @Then("^the product is not added to the cart")
    public void productIsNotAddedCorrectly() {
        Assert.assertFalse(productDetailsPage.messageProductAddedIsDisplay());
        productDetailsPage.acceptShoppMessage();
    }
}
