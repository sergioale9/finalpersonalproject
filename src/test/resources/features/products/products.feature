@ProductAutomationPage @UI
Feature: Product automation page
  @AccessToProductPage
  Scenario: A user can access to product page
    Given I am on the home page
    When I go to the product page
    Then the page redirect me to the products page
  @SearchAProduct
  Scenario Outline: A user can search a product using the search box
    Given I am on the home page
    When I go to the product page
    And I search a product typing "<argument>"
    Then the page return the searched product with "<argument>"
    Examples:
      | argument      |
      | Stylish Dress |
  @ProductDetails
  Scenario Outline: A user can enter to see the details of a product
    Given I am on the home page
    When I go to the product page
    And I select a "<Product>" and enter to see the details
    Then the page redirects to details of the "<Product>"
  Examples:
    | Product  |
    |     5    |
    |     2    |
    |     8    |
  @ProductReview
  Scenario Outline: A user can write a review of a product
    Given I am on the home page
    When I go to the product page
    And I select a "<Product>" and enter to see the details
    Then the page redirects to details of the "<Product>"
    And I fill the review form with the following values
      | Name     | Email           | ReviewBody       |
      | TestUser | testuser@test.com   | This is a review |
    And the form is sent correctly
  Examples:
    | Product  |
    |     5    |
    |     2    |
    |     8    |

  @ProductReview @bug
  Scenario Outline: A user cant sent a review if don't fill all fields
    Given I am on the home page
    When I go to the product page
    And I select a "<Product>" and enter to see the details
    Then the page redirects to details of the "<Product>"
    And I fill the review form with the following values
      | Name     | Email             | ReviewBody       |
      | TestUser |                   |                  |
      |          | testuser@test.com |                  |
      |          |                   | This is a review |
      | TestUser | testuser          | This is a review |
      | TestUser | testuser@test.com |                  |
      | TestUser | This is a review  |                  |
      |          | testuser@test.com | This is a review |
    And the form is not sent correctly
  Examples:
    | Product  |
    |     5    |
    |     2    |
    |     8    |
  @AddProductToCart
  Scenario Outline: A user can add a product to the cart from product details page
    Given I am on the home page
    When I go to the product page
    And I select a "<Product>" and enter to see the details
    Then the page redirects to details of the "<Product>"
    And I add the product to the cart
    And the product is added correctly
  Examples:
      | Product  |
      |     5    |
      |     2    |
      |     8    |
  @AddProductsWithQuantity
  Scenario Outline: A user can set the quantity of products to add to cart
    Given I am on the home page
    When I go to the product page
    And I select a "<Product>" and enter to see the details
    Then the page redirects to details of the "<Product>"
    And I set the quantity of products with the "<value>"
    And I add the product to the cart
    And the product is added correctly
    Examples:
      | Product | value |
      |    5    |   5   |
      |    2    |   1   |
      |    8    |   11  |

  @AddProductsWithQuantity @bug
  Scenario Outline: A user can't add negative quantities value to the cart
    Given I am on the home page
    When I go to the product page
    And I select a "<Product>" and enter to see the details
    Then the page redirects to details of the "<Product>"
    And I set the quantity of products with the "<value>"
    And I add the product to the cart
    And the product is not added to the cart
    Examples:
      | Product | value |
      |    5    |   -5  |
      |    2    |   -1  |
      |    8    |   -11 |
  @AddProductsWithQuantity @bug
  Scenario Outline: A user can't add zero quantity value to the cart
    Given I am on the home page
    When I go to the product page
    And I select a "<Product>" and enter to see the details
    Then the page redirects to details of the "<Product>"
    And I set the quantity of products with the "<value>"
    And I add the product to the cart
    And the product is not added to the cart
    Examples:
      | Product | value |
      |    5    |   0   |
      |    2    |   0   |
      |    8    |   0   |
