@CartAutomationPage @UI
Feature: Cart automation page
  @ShoppingCartIsEmpty
  Scenario: The shopping cart is empty by default for no log in users
    Given I am on the home page
    When I go to the shopping cart without add a product
    Then the shopping cart is empty
  @DeleteProductFromShoppingCart
  Scenario Outline: A user can remove a product of his cart
    Given I am on the home page
    When I add at least one "<Product>" to shopping cart
    Then I go to the shopping cart with "<Product>" added
    And I remove a "<Product>" of the cart
    And the shopping cart is empty
    Examples:
      | Product |
      |    5    |
      |    3    |
      |    1    |
  @CorrectDescriptionAndPrice
  Scenario Outline: The cart page shows the correct description and price of the product
    Given I am on the home page
    When I add at least one "<Product>" to shopping cart
    Then I go to the shopping cart with "<Product>" added
    And the description and price of the product added is the correctly
    Examples:
      | Product |
      |    5    |
      |    3    |
      |    1    |
  @DisplaysTotalAmount
  Scenario Outline: The total amount of the cart should be set correctly
    Given I am on the home page
    When I add at least one "<Product>" to shopping cart
    Then I go to the shopping cart with "<Product>" added
    And the total amount should be displayed for the "<Product>"
    Examples:
      | Product |
      |    5    |
      |    3    |
      |    1    |